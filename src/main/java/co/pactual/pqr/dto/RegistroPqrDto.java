package co.pactual.pqr.dto;

import co.pactual.pqr.domain.Pqr;
import lombok.Data;

@Data
public class RegistroPqrDto {
    private String idPqrAntiguo;
    private Pqr pqrNuevo;
}
