package co.pactual.pqr.exception;

public class ErrorProcessException extends RuntimeException{
    public ErrorProcessException(String message){
        super(message);
    }

    public ErrorProcessException(Throwable ex, String message){
        super(message,ex);
    }
}
