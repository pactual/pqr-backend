package co.pactual.pqr.exception;

import lombok.Data;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public class RestException extends RuntimeException {

    private HttpStatus status;
    private String messageRta;

    public RestException(HttpStatus status, String messageRta) {
        super(messageRta);
        this.status = status;
        this.messageRta = messageRta;
    }
}
