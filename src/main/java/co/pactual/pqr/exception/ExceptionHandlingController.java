package co.pactual.pqr.exception;


import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.concurrent.CompletionException;


@Slf4j
@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {



    @ExceptionHandler(RestException.class)
    @ResponseBody
    public ResponseEntity<Object> erroresControlados(RestException ex, WebRequest request) {
        ApiError apiError = new ApiError(ex.getStatus(), ex.getMessage());
        return new ResponseEntity(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(ErrorProcessException.class)
    public ResponseEntity<Object> erroresProcess(ErrorProcessException ex, WebRequest request) {
        return handleExceptionInternal(ex, "{'error':" + ex.getMessage() + "}",
                new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> erroresNoControlados(Exception ex, WebRequest request) {
        logger.error("Se presneto un error",ex);
        if (ex instanceof CompletionException) {
            CompletionException ex1 = (CompletionException) ex;
            if (ex1.getCause() instanceof RestException) {
                RestException rex = (RestException) ex1.getCause();
                ApiError apiError = new ApiError(rex.getStatus(), rex.getMessage());

                return new ResponseEntity(apiError, new HttpHeaders(), apiError.getStatus());
            }
        }

        ApiError apiError = new ApiError(HttpStatus.INTERNAL_SERVER_ERROR, "Error en el servidor");
        return handleExceptionInternal(ex, apiError, null, HttpStatus.INTERNAL_SERVER_ERROR, request);

    }

}
