package co.pactual.pqr.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Slf4j
@Getter
@Setter
@ToString
public class ApiError {
    @JsonIgnore
    private LocalDateTime timestamp = LocalDateTime.now();
    private HttpStatus status;
    private String message;

    public ApiError(HttpStatus status, String message) {
        this.message = message;
        this.status = status;
        log.info(message);
        log.info(status.getReasonPhrase());
    }
}
