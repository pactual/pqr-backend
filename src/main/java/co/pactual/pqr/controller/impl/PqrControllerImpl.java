package co.pactual.pqr.controller.impl;

import co.pactual.pqr.business.PqrListarBusiness;
import co.pactual.pqr.business.PqrRegistroBusiness;
import co.pactual.pqr.controller.PqrController;
import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.dto.RegistroPqrDto;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RequestMapping("/pqr")
@RestController
@AllArgsConstructor
public class PqrControllerImpl implements PqrController {

    @Qualifier("PqrQuejaRegistro")
    private PqrRegistroBusiness registroQueja;
    @Qualifier("PqrReclamoRegistro")
    private PqrRegistroBusiness registroReclamo;
    @Qualifier("PqrPeticionRegistro")
    private PqrRegistroBusiness registroPeticion;
    private PqrListarBusiness pqrListarBusiness;

    @GetMapping
    public List<Pqr> buscarPorCliente(@RequestParam("identificacionCli") String id){
        return pqrListarBusiness.listarPorCliente(id);
    }

    @GetMapping("/{id}")
    public Pqr buscarPorId(@PathParam("id") String id){
        return pqrListarBusiness.buscarporId(id);
    }

    @PostMapping("/queja")
    @ResponseBody
    public Pqr registrarQueja(@RequestBody @Validated RegistroPqrDto registroDto){
        return registroQueja.iniciarProceso(registroDto);
    }

    @PostMapping("/reclamo")
    @ResponseBody
    public Pqr registrarReclamo(@RequestBody @Validated RegistroPqrDto registroDto){
        return registroReclamo.iniciarProceso(registroDto);
    }

    @PostMapping("/peticion")
    @ResponseBody
    public Pqr registraPeticion(@RequestBody @Validated RegistroPqrDto registroDto){
        return registroPeticion.iniciarProceso(registroDto);
    }
}
