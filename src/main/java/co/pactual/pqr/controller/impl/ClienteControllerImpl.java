package co.pactual.pqr.controller.impl;

import co.pactual.pqr.business.ClienteConsultaBusiness;
import co.pactual.pqr.controller.ClienteController;
import co.pactual.pqr.domain.Cliente;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;

@RequestMapping("/cliente")
@RestController
@AllArgsConstructor
public class ClienteControllerImpl implements ClienteController {

    private ClienteConsultaBusiness clienteConsultaBusiness;

    @GetMapping
    public Iterable<Cliente> listCliente(){
        return clienteConsultaBusiness.listarTodo();
    }

    @GetMapping("/{identificacion}")
    public Cliente buscarPorIdentificacion(@PathParam("identificacion") String identificacion){
        return clienteConsultaBusiness.buscarPorIdentificacion(identificacion);
    }
}
