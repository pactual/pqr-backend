package co.pactual.pqr.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConvertedEnum;
import lombok.Data;

import java.util.Date;

@Data
@DynamoDBDocument
public class PqrResumen {
    @DynamoDBAttribute
    private String idPqr;
    @DynamoDBAttribute
    private Date fechaResumen;
    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private Estado estado;
    @DynamoDBAttribute
    @DynamoDBTypeConvertedEnum
    private TipoPqr tipoPqr;
}
