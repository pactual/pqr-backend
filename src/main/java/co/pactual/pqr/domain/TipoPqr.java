package co.pactual.pqr.domain;

public enum TipoPqr {
    PETICION,
    QUEJA,
    RECLAMO;
}
