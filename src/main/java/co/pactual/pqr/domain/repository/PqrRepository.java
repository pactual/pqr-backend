package co.pactual.pqr.domain.repository;

import co.pactual.pqr.domain.Pqr;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

@EnableScan
public interface PqrRepository extends CrudRepository<Pqr,String> {
    List<Pqr> findByIdCliente(String idCliente);
}
