package co.pactual.pqr.domain.repository;


import co.pactual.pqr.domain.Cliente;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

@EnableScan
public interface ClienteRepository extends CrudRepository<Cliente,String> {

    Optional<Cliente> findByIdentificacion(String s);
}
