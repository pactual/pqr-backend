package co.pactual.pqr.domain;

public enum Estado {
    PENDIENTE_REVISION,
    FINALIZADO;
}
