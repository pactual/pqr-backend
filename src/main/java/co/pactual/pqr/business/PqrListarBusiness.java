package co.pactual.pqr.business;

import co.pactual.pqr.domain.Pqr;

import java.util.List;

public interface PqrListarBusiness {

    List<Pqr> listarPorCliente(String identificacion);

    Pqr buscarporId(String idPqr);
}
