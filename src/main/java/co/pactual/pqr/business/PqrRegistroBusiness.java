package co.pactual.pqr.business;

import co.pactual.pqr.domain.Cliente;
import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.dto.RegistroPqrDto;
import co.pactual.pqr.exception.RestException;
import co.pactual.pqr.services.ClienteService;
import co.pactual.pqr.services.PqrServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public abstract class PqrRegistroBusiness {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    protected PqrServices pqrServices;
    public Pqr iniciarProceso(RegistroPqrDto pqrDto){
        Cliente cliente =clienteService.buscarPorIdentificacion(pqrDto.getPqrNuevo().getIdCliente()).orElseThrow(
                () ->new RestException(HttpStatus.CONFLICT, "Cliente no existe"));
        pqrDto.getPqrNuevo().setIdCliente(cliente.getIdCliente());
        Pqr pqrAntiguo = (pqrDto.getIdPqrAntiguo() !=null)?pqrServices.buscarPorId(pqrDto.getIdPqrAntiguo()).orElse(new Pqr()):new Pqr();
        return this.registrar(pqrAntiguo,pqrDto.getPqrNuevo());
    }

    protected abstract Pqr registrar(Pqr pqr, Pqr nuevo);


}
