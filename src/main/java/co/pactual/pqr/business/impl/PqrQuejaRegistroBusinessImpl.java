package co.pactual.pqr.business.impl;

import co.pactual.pqr.business.PqrRegistroBusiness;
import co.pactual.pqr.domain.Estado;
import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.domain.TipoPqr;
import co.pactual.pqr.services.PqrServices;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@AllArgsConstructor
@Qualifier("PqrQuejaRegistro")
public class PqrQuejaRegistroBusinessImpl extends PqrRegistroBusiness {

    private PqrServices pqrServices;

    protected Pqr registrar(Pqr pqr, Pqr nuevo){
        nuevo.setIdPqr(null);
        nuevo.setFechaRegistro(new Date());
        nuevo.setEstado(Estado.PENDIENTE_REVISION);
        nuevo.setTipoPqr(TipoPqr.QUEJA);
        return pqrServices.almacenar(nuevo);
    }
}
