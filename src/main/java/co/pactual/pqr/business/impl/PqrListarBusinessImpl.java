package co.pactual.pqr.business.impl;

import co.pactual.pqr.business.PqrListarBusiness;
import co.pactual.pqr.domain.Cliente;
import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.exception.RestException;
import co.pactual.pqr.services.ClienteService;
import co.pactual.pqr.services.PqrServices;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PqrListarBusinessImpl implements PqrListarBusiness {

    private PqrServices pqrServices;
    private ClienteService clienteService;

    public List<Pqr> listarPorCliente(String identificacion){
        Cliente cliente = clienteService.buscarPorIdentificacion(identificacion)
                .orElseThrow(() -> new RestException(HttpStatus.CONFLICT,"Cliente no existe"));
        return pqrServices.buscarPorCliente(cliente.getIdCliente());
    }

    public Pqr buscarporId(String idPqr){
        return pqrServices.buscarPorId(idPqr)
                .orElseThrow(()-> new RestException(HttpStatus.CONFLICT,"Pqr no existe"));
    }
}
