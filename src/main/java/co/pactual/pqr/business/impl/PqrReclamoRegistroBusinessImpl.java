package co.pactual.pqr.business.impl;

import co.pactual.pqr.business.PqrRegistroBusiness;
import co.pactual.pqr.domain.Estado;
import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.domain.PqrResumen;
import co.pactual.pqr.domain.TipoPqr;
import co.pactual.pqr.exception.RestException;
import co.pactual.pqr.services.PqrServices;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

@Service
@AllArgsConstructor
@Qualifier("PqrReclamoRegistro")
public class PqrReclamoRegistroBusinessImpl extends PqrRegistroBusiness {

    private PqrServices pqrServices;
    private static final int DIAS_VALIDOS = -5;

    protected Pqr registrar(Pqr pqr, Pqr nuevo){
        pqr = pqrServices.buscarPorId(pqr.getIdPqr()).orElseThrow(()-> new RestException(HttpStatus.CONFLICT,"No existe pqr"));
        if(this.tipoValidoParaRegistroReclamo(pqr) &&
            this.estadoValidoParaRegistroReclamo(pqr)){
            if(pqr.getResumen() == null){
                pqr.setResumen(new ArrayList<>());
            }else {
                pqr.getResumen()
                        .stream().filter(pqrRes -> pqrRes.getTipoPqr().equals(TipoPqr.RECLAMO)
                                                    && pqrRes.getEstado().equals(Estado.PENDIENTE_REVISION))
                        .findFirst().orElseThrow(()-> new RestException(HttpStatus.CONFLICT,"Ya tiene un PQR de reclamos que se encuentra abierto"));
            }

            nuevo.setFechaRegistro(new Date());
            nuevo.setEstado(Estado.PENDIENTE_REVISION);
            nuevo.setTipoPqr(TipoPqr.RECLAMO);
            nuevo = pqrServices.almacenar(nuevo);

            PqrResumen pqrResumen = new PqrResumen();
            pqrResumen.setIdPqr(nuevo.getIdPqr());
            pqrResumen.setFechaResumen(nuevo.getFechaRegistro());
            pqrResumen.setTipoPqr(nuevo.getTipoPqr());
            pqrResumen.setEstado(nuevo.getEstado());
            pqr.getResumen().add(pqrResumen);
            pqrServices.almacenar(pqr);
            return nuevo;
        }else{
            throw new RestException(HttpStatus.CONFLICT,"PQR Invalido para registrar una reclamo.");
        }
    }

    private boolean tipoValidoParaRegistroReclamo(Pqr pqr){
        return TipoPqr.PETICION.equals(pqr.getTipoPqr())|| TipoPqr.QUEJA.equals(pqr.getTipoPqr());
    }

    private boolean estadoValidoParaRegistroReclamo(Pqr pqr){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH,DIAS_VALIDOS);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.SECOND,0);
        calendar.set(Calendar.MILLISECOND,0);
        return  Estado.FINALIZADO.equals(pqr.getEstado()) || calendar.getTime().compareTo(pqr.getFechaRegistro()) >= 0;

    }

}
