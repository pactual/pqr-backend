package co.pactual.pqr.business.impl;

import co.pactual.pqr.business.ClienteConsultaBusiness;
import co.pactual.pqr.domain.Cliente;
import co.pactual.pqr.exception.RestException;
import co.pactual.pqr.services.ClienteService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class ClienteConsultaBusinessImpl implements ClienteConsultaBusiness {
    private ClienteService clienteService;

    @Override
    public Iterable<Cliente> listarTodo(){
        return clienteService.listarClientes();
    }

    @Override
    public Cliente buscarPorIdentificacion(String identificacion){
        return clienteService.buscarPorIdentificacion(identificacion)
                .orElseThrow(() -> new RestException(HttpStatus.CONFLICT,"Cliente no existe"));
    }
}
