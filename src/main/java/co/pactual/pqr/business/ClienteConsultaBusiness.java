package co.pactual.pqr.business;

import co.pactual.pqr.domain.Cliente;

import java.util.Optional;

public interface ClienteConsultaBusiness {
    Iterable<Cliente> listarTodo();

    Cliente buscarPorIdentificacion(String identificacion);
}
