package co.pactual.pqr.services.impl;

import co.pactual.pqr.domain.Cliente;
import co.pactual.pqr.domain.repository.ClienteRepository;
import co.pactual.pqr.services.ClienteService;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ClienteServiceImpl implements ClienteService {
    private ClienteRepository clienteRepository;

    public Iterable<Cliente> listarClientes(){
        try {
            return clienteRepository.findAll();
        }catch (ResourceNotFoundException ex){
            ex.printStackTrace();
            return new ArrayList<>();
        }
    }


    public Optional<Cliente> buscarPorIdentificacion(String identificacion){
        return clienteRepository.findByIdentificacion(identificacion);
    }
}
