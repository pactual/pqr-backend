package co.pactual.pqr.services.impl;

import co.pactual.pqr.domain.Pqr;
import co.pactual.pqr.domain.repository.PqrRepository;
import co.pactual.pqr.services.PqrServices;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PqrServicesImpl implements PqrServices {
    private PqrRepository pqrRepository;

    @Override
    public List<Pqr> buscarPorCliente(String idCliente){
        return pqrRepository.findByIdCliente(idCliente);
    }

    @Override
    public Optional<Pqr> buscarPorId(String id){
        return pqrRepository.findById(id);
    }

    @Override
    public Pqr almacenar(Pqr pqr){
        return pqrRepository.save(pqr);
    }
}
