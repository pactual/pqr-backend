package co.pactual.pqr.services;

import co.pactual.pqr.domain.Pqr;

import java.util.List;
import java.util.Optional;

public interface PqrServices {

    List<Pqr> buscarPorCliente(String idCliente);

    Optional<Pqr> buscarPorId(String id);

    Pqr almacenar(Pqr pqr);
}
