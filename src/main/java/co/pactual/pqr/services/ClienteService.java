package co.pactual.pqr.services;

import co.pactual.pqr.domain.Cliente;

import java.util.Optional;

public interface ClienteService {
    Iterable<Cliente> listarClientes();

    Optional<Cliente> buscarPorIdentificacion(String identificacion);
}
